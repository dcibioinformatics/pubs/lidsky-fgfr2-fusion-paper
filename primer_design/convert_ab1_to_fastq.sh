#!/bin/bash

# singularity exec  docker://geargenomics/tracy
OUTDIR=./fastq_files
mkdir -p $OUTDIR

for CUR_AB1 in ./AB1_files/*.ab1
do
    echo $CUR_AB1
    SAMPLE_NAME=$(basename ${CUR_AB1} ".ab1")
    echo $SAMPLE_NAME
    FASTQ_NAME=${OUTDIR}/${SAMPLE_NAME}.fastq
    echo $FASTQ_NAME
    singularity exec  docker://geargenomics/tracy tracy basecall -f fastq -o $FASTQ_NAME $CUR_AB1
done

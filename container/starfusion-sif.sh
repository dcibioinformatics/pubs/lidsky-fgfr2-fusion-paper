#!/bin/bash

VERSION=1.10.1

singularity build star-fusion.v${VERSION}.sif docker://trinityctat/starfusion:$VERSION

singularity exec star-fusion.v${VERSION}.sif /usr/local/src/STAR-Fusion/STAR-Fusion --version

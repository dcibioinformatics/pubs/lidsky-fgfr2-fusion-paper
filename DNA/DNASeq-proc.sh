#!/bin/bash

preprocess() {
    local stem=$1
    local locfq=$2
    local R1=$3
    local R2=$4
    local procdir=$5

    local bwasam=${stem}.sam
    local bwaerr=${stem}-bwa.err
    
    local RGbam=${stem}-RG.bam
    local RGout=${stem}-RG.out
    local RGerr=${stem}-RG.err

    local dedupbam=${stem}-dedup.bam
    local dupmetrics=${stem}-dedup-metrics.txt
    local dupout=${stem}-dedup.out
    local duperr=${stem}-dedup.err

    local recaltab=${stem}-recal.table
    local baseout=${stem}-base.out
    local baseerr=${stem}-base.err
    
    local recalbam=${stem}-recal.bam
    local recalout=${stem}-bqsr.out
    local recalerr=${stem}-bqsr.err

    ${singularity} exec --app bwa \
     		   --bind=${locfq}:/cntfq/,${locanno}:/cntanno/ \
     		   ${cntsif} \
     	           bwa mem -M -t 40 /cntanno/${reffasta} /cntfq/${R1} /cntfq/${R2} \
    		   > ${procdir}${bwasam} 2> ${procdir}${bwaerr}

    ${singularity} exec --app picardtools \
    		   --bind=${procdir}:/cntproc/,${loctmp}:/cnttmp/ \
    		   ${cntsif} \
    		   java -Xmx16G -Djava.io.tmpdir=/cnttmp/${tmpdir} \
    		   -jar /scif/apps/picardtools/picard.jar \
    		   AddOrReplaceReadGroups \
    		   I=/cntproc/${bwasam} \
    		   O=/cntproc/${RGbam} \
    		   SO=coordinate \
    		   CREATE_INDEX=true \
    		   RGID=${stem}ID \
    		   RGLB=${stem}LB \
    		   RGPL=ILLUMINA \
    		   RGPU=${stem}PU \
    		   RGSM=${stem}SM \
    		   > ${procdir}${RGout} 2> ${procdir}${RGerr}

    ${singularity}  exec --app picardtools \
    		   --bind=${procdir}:/cntproc/,${loctmp}:/cnttmp/ \
    		   ${cntsif} \
    		   java -Xmx16G -Djava.io.tmpdir=/cnttmp/${tmpdir} \
    		   -jar /scif/apps/picardtools/picard.jar \
    		   MarkDuplicates \
    		   I=/cntproc/${RGbam} \
    		   O=/cntproc/${dedupbam} \
    		   M=/cntproc/${dupmetrics} \
    		    > ${procdir}${dupout} 2> ${procdir}${duperr}

    ${singularity} exec --app gatk \
    		   --bind=${procdir}:/cntproc/,${loctmp}:/cnttmp/,${locanno}:/cntanno/ \
    		   ${cntsif} \
    		   gatk  ${gatk} --java-options "-Xmx16G" BaseRecalibrator \
    		   --reference /cntanno/${reffasta} \
    		   --input /cntproc/${dedupbam} \
    		   --use-original-qualities \
    		   --output /cntproc/${recaltab} \
    		   --known-sites /cntanno/${snp1} \
    		   --known-sites /cntanno/${indel1} \
    		   --known-sites /cntanno/${indel2} \
    		   --tmp-dir /cnttmp/ \
    		   > ${procdir}${baseout} 2> ${procdir}${baseerr}

    ${singularity} exec --app gatk \
    		   --bind=${procdir}:/cntproc/,${loctmp}:/cnttmp/,${locanno}:/cntanno/ \
    		   ${cntsif} \
    		   gatk  ${gatk} --java-options "-Xmx16G" ApplyBQSR \
    		   --input /cntproc/${dedupbam} \
    		   --output /cntproc/${recalbam} \
    		   --bqsr-recal-file /cntproc/${recaltab} \
    		   --add-output-sam-program-record \
    		   --create-output-bam-md5 \
    		   --create-output-bam-index \
    		   --tmp-dir /cnttmp/ \
    		   > ${procdir}${recalout} 2> ${procdir}${recalerr}

}




# Singularity sif file
singularity=/usr/bin/singularity
cntsif=/mnt/data1/workspace/Lidsky/FGFR2/Submission/Containers/container.sif

# Set location of tmp directory
loctmp=/mnt/data1/workspace/Lidsky/FGFR2/tmp/


# Annotation and reference files

## Specify local directory containing GATK bundle reference and annotation
## files (download from https://console.cloud.google.com/storage/browser/gcp-public-data--broad-references/hg38/v0)
locanno=/mnt/data2/Annotation/GATK/hg38/v0/
reffasta=Homo_sapiens_assembly38.fasta
snp1=Homo_sapiens_assembly38.dbsnp138.vcf
indel1=Mills_and_1000G_gold_standard.indels.hg38.vcf.gz
indel2=Homo_sapiens_assembly38.known_indels.vcf.gz 

# Process DUC18828

## Set local directory containing R1/R2 fastq files
locfq=/mnt/data1/workspace/Lidsky/FGFR2/Submission/Data/DNA-Seq/Sample_DUC18828_IGO_10973_1/
## Set R1/R2 file names
R1=DUC18828_IGO_10973_1_S12_R1_001.fastq.gz
R2=DUC18828_IGO_10973_1_S12_R2_001.fastq.gz
stemlab=DUC18828
## Pre-process sample
procdir=/mnt/data1/workspace/Lidsky/FGFR2/Submission/Output/DNA/DUC18828/
mkdir -p ${procdir}
preprocess ${stemlab} ${locfq} ${R1} ${R2} ${procdir}


# Process DUC19640
## Set local directory containing R1/R2 fastq files
locfq=/mnt/data1/workspace/Lidsky/FGFR2/Submission/Data/DNA-Seq/Sample_DUC19640_IGO_10973_2/
## Set R1/R2  file names
R1=DUC19640_IGO_10973_2_S13_R1_001.fastq.gz
R2=DUC19640_IGO_10973_2_S13_R2_001.fastq.gz
stemlab=DUC19640
## Pre-process sample
procdir=/mnt/data1/workspace/Lidsky/FGFR2/Submission/Output/DNA/DUC19640/
mkdir -p ${procdir}
preprocess ${stemlab} ${locfq} ${R1} ${R2} ${procdir}


#!/bin/bash
set -x

dellyproc() {
    local stem=$1
    local recalbam=$2
    local outdir=${procdir}${stem}/

    bcffile=${stem}-delly.bcf

    
    ${singularity} exec --app dellytools \
    		   --bind ${outdir}:/cntproc/ \
		   --bind ${dellyanno}:/cntdelly/ \
		   --bind ${locref}:/cntref/ \
    		   ${cntsif} \
    		   delly call \
    		   -x /cntdelly/${refexcl} \
    		   -o /cntproc/${bcffile} \
    		   -g /cntref/${reffasta} \
    		   /cntproc/${recalbam} \
    		   > ${outdir}${stem}-delly.out 2> ${outdir}${stem}-delly.err 

    ${singularity} exec --app dellytools \
		   --bind ${outdir}:/cntproc/ \
		   --bind ${dellyanno}:/cntdelly/ \
		   --bind ${locref}:/cntref/ \
		   ${cntsif} \
		   sansa annotate \
		   --gtf /cntdelly/${gtffile} \
		   --feature gene \
		   --id gene_name \
		   --anno /cntproc/${stem}-sansa-geneanno.bcf \
		   --output /cntproc/${stem}-sansa-geneanno.tsv.gz \
		   /cntproc/${bcffile} \
		   > ${outdir}${stem}-gene-sansa.out 2> ${outdir}${stem}-gene-sansa.err

    ${singularity} exec --app dellytools \
                   --bind ${outdir}:/cntproc/ \
		   --bind ${dellyanno}:/cntdelly/ \
		   --bind ${locref}:/cntref/ \
                   ${cntsif} \
                   sansa annotate \
                   --gtf /cntdelly/${gtffile} \
                   --feature exon \
		   --id exon_id \
                   --anno /cntproc/${stem}-sansa-exonanno.bcf \
                   --output /cntproc/${stem}-sansa-exonanno.tsv.gz \
                   /cntproc/${bcffile} \
                   > ${outdir}${stem}-exon-sansa.out 2> ${outdir}${stem}-exon-sansa.err

    
}


# Singularity sif file
singularity=/usr/bin/singularity
cntsif=/mnt/data1/workspace/Lidsky/FGFR2/Submission/Containers/container.sif

# Annotation and reference files

## Specify local directory containing GATK bundle reference and annotation
## files (download from https://console.cloud.google.com/storage/browser/gcp-public-data--broad-references/hg38/v0)
locref=/mnt/data2/Annotation/GATK/hg38/v0/
reffasta=Homo_sapiens_assembly38.fasta

dellyanno=/mnt/data1/workspace/Lidsky/FGFR2/Submission/Annotation/
# From https://github.com/dellytools/delly/tree/main/excludeTemplates
refexcl=human.hg38.excl.tsv
# From https://ftp.ebi.ac.uk/pub/databases/gencode/Gencode_human/release_39/gencode.v39.primary_assembly.annotation.gtf.gz
gtffile=gencode.v39.primary_assembly.annotation.gtf.gz

procdir=/mnt/data1/workspace/Lidsky/FGFR2/Submission/Output/DNA/

dellyproc DUC18828 DUC18828-recal.bam
dellyproc DUC19640 DUC19640-recal.bam

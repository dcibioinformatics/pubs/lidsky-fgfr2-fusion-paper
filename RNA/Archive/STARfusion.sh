#!/bin/bash

# STAR FUSION sif file
siffile='../container/star-fusion.v1.10.1.sif'
singularity exec ${siffile} /usr/local/src/STAR-Fusion/STAR-Fusion --version
siffile=/mnt/data2/studydata/lidsky/fgfr2_rnaseq/RawData/LU_6786_210305B7/Code/STAR-Fusion-v1.10.0/Docker/star-fusion.v1.10.0.simg
# CTAT resource
CTATlib=/mnt/data2/Annotation/CTAT/GRCh38_gencode_v37_CTAT_lib_Mar012021.plug-n-play/ctat_genome_lib_build_dir/
CTATlib=/mnt/data2/studydata/lidsky/fgfr2_rnaseq/RawData/LU_6786_210305B7/ctat-genome-lib/GRCh38_gencode_v37_CTAT_lib_Mar012021.plug-n-play/ctat_genome_lib_build_dir/
# FASTQ dir
fqdir='/mnt/data1/workspace/Lidsky/FGFR2/Paper/Data/RNA-Seq/LU_6786_210305B7/'
# Output directory
procdir='./PROC/'

for stem in DUC18828-1_S9 DUC18828-2_S10 DUC18828-3_S11;do
    echo ${stem}
    reads_1=${fqdir}/${stem}_L002_R1_001.fastq.gz
    reads_2=${fqdir}/${stem}_L002_R2_001.fastq.gz
    ls -lsa ${reads_1}
    ls -lsa ${reads_2}
    mkdir -p ${procdir}/${stem}/
    singularity exec \
		-B ${CTATlib} \
		-B ${reads_1}:/reads_1.fq.gz \
		-B ${reads_2}:/reads_2.fq.gz \
		-B ${procdir}/${stem}:/output \
		${siffile} \
                /usr/local/src/STAR-Fusion/STAR-Fusion \
		--genome_lib_dir ${CTATlib} \
                --left_fq /reads_1.fq.gz \
                --right_fq /reads_2.fq.gz \
                --output_dir /output \
                --FusionInspector validate \
                --examine_coding_effect   \
                --denovo_reconstruct \
		--CPU 20
done

#!/bin/bash

# Merge three paired-end RNA-Seq samples from DUC18828 assuming
# that the three samples are technical replicates

cntsif='/mnt/data1/workspace/Lidsky/FGFR2/Submission/Containers/container.sif'
valfqjar='/scif/apps/validatefastq/validatefastq-assembly-0.1.1.jar'

procdir=/mnt/data1/workspace/Lidsky/FGFR2/Submission/Output/RNA/DUC18828/
fqdir=/mnt/data1/workspace/Lidsky/FGFR2/Submission/Data/RNA-Seq/LU_6786_210305B7/


#' Merge fastq files
cat \
	${fqdir}DUC18828-1_S9_L002_R1_001.fastq.gz \
	${fqdir}DUC18828-2_S10_L002_R1_001.fastq.gz \
	${fqdir}DUC18828-3_S11_L002_R1_001.fastq.gz > ${procdir}DUC18828-R1.fastq.gz

cat \
	${fqdir}DUC18828-1_S9_L002_R2_001.fastq.gz \
	${fqdir}DUC18828-2_S10_L002_R2_001.fastq.gz \
	${fqdir}DUC18828-3_S11_L002_R2_001.fastq.gz > ${procdir}DUC18828-R2.fastq.gz

#' Validate merged PE fastq pair
singularity exec ${cntsif} java -jar ${valfqjar} --version


singularity \
	exec \
	--bind ${procdir}:/mnt/procdir/ \
	${cntsif} \
	java -jar ${valfqjar} \
	-i /mnt/procdir/DUC18828-R1.fastq.gz \
	-j /mnt/procdir/DUC18828-R2.fastq.gz &> ${procdir}/DUC18828-validatefastq.log

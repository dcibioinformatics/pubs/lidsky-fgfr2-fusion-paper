#!/bin/bash

# Run STAR Fusion pipeline on merged DUC18828 RNA-seq samples
# may need to increase ulimit ($ ulimit -n 10000)

# Container sif file directory
cntdir=/mnt/data1/workspace/Lidsky/FGFR2/Submission/Containers/
## Container for samtools
cntsif=${cntdir}container.sif
## Container for STAR Fusion
starfusionsif=${cntdir}star-fusion.v1.10.1.sif


# CTAT library
CTATlib='/mnt/data2/Annotation/CTAT/GRCh38_gencode_v37_CTAT_lib_Mar012021.plug-n-play/ctat_genome_lib_build_dir/'

# Output directory
procdir=/mnt/data1/workspace/Lidsky/FGFR2/Submission/Output/RNA/DUC18828/


# Check STAR Fusion Version
singularity exec ${starfusionsif} /usr/local/src/STAR-Fusion/STAR-Fusion --version

ulimit -n 10000
# Run STAR Fusion pipeline
singularity exec \
    --bind ${procdir}:/mnt/procdir/ \
    --bind ${CTATlib}:/mnt/ctat/ \
    ${starfusionsif} \
    /usr/local/src/STAR-Fusion/STAR-Fusion \
    --genome_lib_dir /mnt/ctat/ \
    --left_fq  /mnt/procdir/DUC18828-R1.fastq.gz \
    --right_fq /mnt/procdir/DUC18828-R2.fastq.gz \
    --output_dir /mnt/procdir/ \
    --STAR_SortedByCoordinate \
    --FusionInspector validate \
    --examine_coding_effect \
    --denovo_reconstruct \
    --CPU 40

# Index bam file
singularity exec --app samtools ${cntsif} samtools --version

singularity exec \
    --app samtools \
    --bind ${procdir}:/mnt/procdir/ \
    ${cntsif} \
    samtools index -b -@ 20 /mnt/procdir/Aligned.sortedByCoord.out.bam
